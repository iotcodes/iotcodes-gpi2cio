# /////////////////////////////////////////////////////////////////////////////
# IOTCODES.ORG
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being covered by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////

#!/bin/bash

# INIT:
CURRENT_DIR="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_DIR}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"

figlet -w 240 "${SCRIPT_NAME}..." 2> /dev/null
if [ $? -ne 0 ]; then
    banner "${SCRIPT_NAME}..." 2> /dev/null
    if [ $? -ne 0 ]; then
    	echo "> ${SCRIPT_NAME}:" | tr a-z A-Z 
	fi
fi
echo 

# MAIN:
buildDir="build"
cd "${SCRIPT_PATH}/${buildDir}"

# MAKE:
echo "> Flashing device..."
set +e
make flash
exitCode=$?
if [ $exitCode -ne 0 ]; then
	echo ""
	echo "> Flashing device failed with exit code <${exitCode}>, aborting."
	echo "> You have to specify '--flash-port' and '--upload-later' before" 
	echo "> uploading when using the build script. Build as follows beforehand:"
	echo "$ ./build.sh --flash-port <flashPort> --upload-later"
	exit 1
fi
echo "> Flashing device (ok)."

# DONE:
echo "> Done (ok)."
