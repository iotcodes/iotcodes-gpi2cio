# ------------------------------------------------------------------------------
# BOARD OPTIONS:
# ------------------------------------------------------------------------------
set(MCU "atmega2560" CACHE STRING "Processor Type")
set(CPU_SPEED "16000000" CACHE STRING "Speed of the CPU")
set(PORT_SPEED "115200" CACHE STRING "Serial Port Speed")
set(PIN_VARIANT "standard" CACHE STRING "Pin Selection Variant. Either standard, mega, leonardo, eightanaloginputs")
set(PROGRAMMER "stk500v2" CACHE STRING "Programmer Type")