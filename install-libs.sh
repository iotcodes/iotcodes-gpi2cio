# /////////////////////////////////////////////////////////////////////////////
# IOTCODES.ORG
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being covered by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////

#!/bin/bash

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

CURRENT_DIR="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_DIR}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"

figlet -w 240 "${SCRIPT_NAME}..." 2> /dev/null
if [ $? -ne 0 ]; then
    banner "${SCRIPT_NAME}..." 2> /dev/null
    if [ $? -ne 0 ]; then
    	echo "> ${SCRIPT_NAME}:" | tr a-z A-Z 
	fi
fi

# ------------------------------------------------------------------------------
# VARIABLES:
# ------------------------------------------------------------------------------

DEPS_DIR="${SCRIPT_PATH}/libs"
DEST_DIR=$DEPS_DIR

# ------------------------------------------------------------------------------
# FUNCTIONS:
# ------------------------------------------------------------------------------

function probe {
	path=$(which $1 2> /dev/null )
	if [ $? -ne 0 ]; then
		echo "> Cannot invoke '$1', please install required tool '$1', aborting."
		exit 1
	fi
}

# ------------------------------------------------------------------------------

function ask {
	input=""
	while ([ "$input" !=  "n" ] && [ "$input" !=  "y" ] ); do
	  echo -n "> Continue? Enter [n] to quit, [y] to continue: "; read input;
	done
	if [ "$input" == "n" ] ; then
		echo "> Aborting due to user input."
		exit 1
	fi
}

# ------------------------------------------------------------------------------

function clone {
	method=$1
	packageUrl=$2
	package=$3
	repository=${packageUrl##*/}
	gitUrl="${packageUrl}.git"
	echo "> Installing '${repository}' from '${gitUrl}'..."
	if [ -d "${DEPS_DIR}/${repository}" ]; then
		echo "> The folder '${DEPS_DIR}/${repository}' already exists, skipping this repository..."
		return
	fi
	
	if [ ! -d "${DEST_DIR}/${repository}" ]; then
		result=$(GIT_SSL_NO_VERIFY=true git clone "${gitUrl}" "${DEST_DIR}/${repository}" 2>&1)
		if [ $? -ne 0 ]; then
			echo "${result}"
			echo "> Cannot clone '${gitUrl}' as of above error(s), aborting."
			rm -Rf "${DEST_DIR}/${repository}"
			exit 1
		fi
	else
		echo "> The folder '${DEST_DIR}/${repository}' already exists, skipping clone..."
	fi
	
	# BRANCH |-->
	# if [ -n "${branch}" ] ; then
	# 	cd "${repository}"
	# 	result=$(git checkout "${branch}" 2>&1)
	# 	if [ $? -ne 0 ]; then
	# 		echo "${result}"
	# 		echo "> Cannot checkout branch '${branch}' for '${gitUrl}' as of above error(s), aborting."
	# 		exit 1
	# 	fi
	# 	cd ..
	# fi
	# <--| BRANCH 
	
	# RENAME |-->
	if [ -n "${package}" ] ; then
		if  [ "${repository}" != "${package}" ] ; then
			if [ -d "${DEST_DIR}/${package}" ] ; then
				echo "> Orphaned '${DEST_DIR}/${package}' found, cannot rename '${factoryFolder}' in '${DEST_DIR}', aborting."
				exit 1	
			fi
			result=$(mv "${DEST_DIR}/${repository}" "${DEST_DIR}/${package}" 2>&1)
			if [ $? -ne 0 ]; then
				echo "${result}"
				echo "> Cannot rename '${repository}' to '${package}' in '${DEST_DIR}', aborting."
				exit 1
			fi
		fi
	else
		package="${repository}" 
	fi
	# <--| RENAME

	# POST-PROCESS |-->
	postSymlink "${package}"
	postPatch "${package}"
	postInstall "${package}"
	# POST-PROCESS <--|
}

# ------------------------------------------------------------------------------

function download {
	method=$1
	packageUrl=$2
	package=$3
	factoryFolder=$4
	
	archive="$(basename ${packageUrl})"
	
	if [ -z "${package}" ] ; then
		package=${factoryFolder}
	fi
	echo "> Installing '${package}' from '${packageUrl}'..."
	if [ -d "${DEPS_DIR}/${package}" ]; then
		echo "> The folder '${DEPS_DIR}/${package}' already exists, skipping this package..."
		return
	fi
	
	# DOWNLOAD |-->
	if [  ! -f "${DEPS_DIR}/${archive}" ] ; then
		result=$(wget --no-check-certificate  "${packageUrl}" -P "${DEPS_DIR}" 2>&1)
		if [ $? -ne 0 ]; then
			echo "${result}"
			echo "> Cannot download '${package}' from '${packageUrl}' as of above error(s), aborting."
			rm "${DEPS_DIR}/${archive}"
			exit 1
		fi
	else
		echo "> The file '${archive}' already exists, skipping download..."
	fi
	# <--| DOWNLOAD

	# UNPACK |-->
	targetDir="${DEST_DIR}"
	purgeDir="${DEST_DIR}/${factoryFolder}"
	if [ -z "${factoryFolder}" ] ; then
		targetDir="${DEST_DIR}/${package}"
		purgeDir="${DEST_DIR}/${package}"
		if [ ! -d "${targetDir}" ] ; then
			result=$(mkdir "${targetDir}" 2>&1)
			if [ $? -ne 0 ]; then
				echo "${result}"
				echo "> Cannot create '${targetDir}' for '${package}', aborting."
				exit 1
			fi
		fi
	fi
	
	if [[ "${method}" = "tar" ]] ; then
		# TAR |-->
		result=$(tar xf "${DEPS_DIR}/${archive}" --directory "${targetDir}" 2>&1)
		if [ $? -ne 0 ]; then
			echo "${result}"
			echo "> Cannot unpack '${factoryFolder}' to '${package}' in '${DEST_DIR}', aborting."
			rm -Rf "${purgeDir}"
			exit 1
			
		fi
		# <--| TAR
	elif [[ "${method}" = "tar.xz" ]] ; then
		# TAR.XZ |-->
		result=$(tar xf "${DEPS_DIR}/${archive}" --directory "${targetDir}" 2>&1)
		if [ $? -ne 0 ]; then
			echo "${result}"
			echo "> Cannot unpack '${factoryFolder}' to '${package}' in '${DEST_DIR}', aborting."
			rm -Rf "${purgeDir}"
			exit 1
			
		fi
		# <--| TAR.XZ
	elif [[ "${method}" = "tar.gz" ]] ; then
		# TAR.GZ |-->
		result=$(tar xzf "${DEPS_DIR}/${archive}" --directory "${targetDir}" 2>&1)
		if [ $? -ne 0 ]; then
			echo "${result}"
			echo "> Cannot unpack '${factoryFolder}' to '${package}' in '${DEST_DIR}', aborting."
			rm -Rf "${purgeDir}"
			exit 1
			
		fi
		# <--| TAR.GZ
	elif [[ "${method}" = "tar.bz2" ]] ; then
		# TAR.BZ2 |-->
		result=$(tar xjf "${DEPS_DIR}/${archive}" --directory "${targetDir}" 2>&1)
		if [ $? -ne 0 ]; then
			echo "${result}"
			echo "> Cannot unpack '${factoryFolder}' to '${package}' in '${DEST_DIR}', aborting."
			rm -Rf "${purgeDir}"
			exit 1
			
		fi
		# <--| TAR.BZ2
	elif [[ "${method}" = "zip" ]] ; then
		# ZIP |-->
		result=$(unzip "${DEPS_DIR}/${archive}" -d "${targetDir}" 2>&1)
		if [ $? -ne 0 ]; then
			echo "${result}"
			echo "> Cannot unpack '${archive}' for '${package}' in '${DEST_DIR}', aborting."
			rm -Rf "${purgeDir}"
			exit 1
		fi
		# <--| ZIP
	fi
	# <--| UNPACK	
	
	# RENAME |-->
	if [ -n "${package}" ] ; then
		if [ -n "${factoryFolder}" ] &&  [ "${factoryFolder}" != "${package}" ] ; then
			if [ -d "${DEST_DIR}/${package}" ] ; then
				echo "> Orphaned '${DEST_DIR}/${package}' found, cannot rename '${factoryFolder}' in '${DEST_DIR}', aborting."
				exit 1	
			fi
			result=$(mv "${DEST_DIR}/${factoryFolder}" "${DEST_DIR}/${package}" 2>&1)
			if [ $? -ne 0 ]; then
				echo "${result}"
				echo "> Cannot rename '${factoryFolder}' to '${package}' in '${DEST_DIR}', aborting."
				exit 1
			fi
		fi
	fi
	# <--| RENAME
	
	# POST-PROCESS |-->
	postSymlink "${package}"
	postPatch "${package}"
	postInstall "${package}"
	# POST-PROCESS <--|
	
	# CLEANUP |-->
	rm "${DEPS_DIR}/${archive}"
	# CLEANUP <--|
}

# ------------------------------------------------------------------------------

function clean {
	package=$3
	if [ -z "${package}" ] ; then
		packageUrl=$2
		package=${packageUrl##*/}
	fi
	if [ -z "${package}" ] ; then
		echo "> Bad library list file, aborting!"
		exit 1
	fi
	echo "> Cleaning up '${package}'..."
	
	# PRE-PROCESS |-->
	preClean "${package}"
	# PRE-PROCESS <--|
	
	result=$(rm -Rf "${DEPS_DIR}/${package}")
	if [ $? -ne 0 ]; then
		echo "${result}"
		echo "> Cannot remove '${package}' from '${DEPS_DIR}' as of above error(s), aborting."
		exit 1
	fi
	if [ "${DEST_DIR}" != "${DEPS_DIR}" ] ; then
		result=$(rm -Rf "${DEST_DIR}/${package}")
		if [ $? -ne 0 ]; then
			echo "${result}"
			echo "> Cannot remove '${package}' from '${DEPS_DIR}' as of above error(s), aborting."
			exit 1
		fi
	fi
}

# ------------------------------------------------------------------------------

function postSymlink {
	package=$1
	if [ "$DEST_DIR" != "$DEPS_DIR" ] ; then
		repoDir="${DEST_DIR}/${package}"
		echo "> Creating symbolic links from '${repoDir}' to '${DEPS_DIR}'..."
		ln -s "${repoDir}" "$DEPS_DIR"
	fi
}

# ------------------------------------------------------------------------------

function postPatch {
	package=$1
	patchFile="${DEPS_DIR}/${package}.patch"
	if [ -f "${patchFile}" ]; then
    	echo "> Patching '${package}'..."
		cd "${DEST_DIR}/${package}"
		result=$(patch -p1 < "${patchFile}" 2>&1)
		if [ $? -ne 0 ]; then
				echo "${result}"
				echo "> Cannot patch '${package}' with patch file '${patchFile}' as of above error(s), aborting."
				exit 1
			fi
		cd ..
	fi
}

# ------------------------------------------------------------------------------

function postInstall {
	package=$1
	postInstallScript="${package}.post-install.sh"
	if [ -f "${DEPS_DIR}/${postInstallScript}" ]; then
    	echo "> Post-processing '${package}'..."
		cd "${DEPS_DIR}"
		result=$("./${postInstallScript}")
		if [ $? -ne 0 ]; then
				echo "${result}"
				echo "> Cannot post-install '${package}' with script '${postInstallScript}' as of above error, aborting."
				exit 1
			fi
		cd ..
	fi
}

# ------------------------------------------------------------------------------

function preClean {
	package=$1
	preCleanScript="${package}.pre-clean.sh"
	if [ -f "${DEPS_DIR}/${preCleanScript}" ]; then
    	echo "> Pre-processing '${package}'..."
		cd "${DEPS_DIR}"
		result=$("./${preCleanScript}")
		if [ $? -ne 0 ]; then
				echo "${result}"
				echo "> Cannot pre-clean '${package}' with script '${preCleanScript}' as of above error, aborting."
				exit 1
			fi
		cd ..
	fi
}

# ------------------------------------------------------------------------------

function help {
	echo "Usage: $SCRIPT_NAME.sh [ -h | -d destinationDir | -c ]"
	echo " -d: The folder where to install your libs to (optional)."
	echo " -c: Clean installed libraries from disk."
	echo " -h: Shows this help."
}

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------

probe "basename"
probe "git"
probe "patch"
probe "tar"
probe "unzip"

args=$@
POSITIONAL=()
while [[ $# -gt 0 ]] ; do
	key="$1"
	case $key in
		-d|--destination)
			if [ -z "$2" ] ; then
				help
				echo "> Wrong (number of) arguments: '$args'"
				exit 1
			fi
			DEST_DIR=${2%/}
			shift
			shift
		;;
		-c|--clean)
			CLEAN="true"
			shift
		;;
		-h|--help)
			help
			exit 0
		;;
		*)
			help
			echo "> Wrong (number of) arguments: '$args'"
			exit 1
		;;
	esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# DEST_DIR |-->
if [ ! -d ${DEST_DIR} ]; then
	echo "The folder '${DEST_DIR}' does not exist, aborting!"
	exit 1
fi
cd "${DEST_DIR}"
if [ $? -ne 0 ]; then
	help
	echo "> Cannot access folder '${DEST_DIR}', aborting."
	exit 1
fi
DEST_DIR="$(pwd)"
cd "${CURRENT_DIR}"
# <--| DEST_DIR

if [[ $CLEAN == "true" ]]; then

	# --------------------------------------------------------------------------
	# CLEAN:
	# --------------------------------------------------------------------------
	
    echo "> Really cleanup any installed libraries?"
	ask
	
	while IFS='' read -r line || [[ -n "$line" ]]; do
		if [[ $line != \#* ]] ; then
			clean ${line}
		fi
	done < "${SCRIPT_PATH}/${SCRIPT_NAME}.lst"

else

	# --------------------------------------------------------------------------
	# INSTALL:
	# --------------------------------------------------------------------------

	echo "> Installing external libraries to folder '${DEST_DIR}'..."
	ask

	while IFS='' read -r line || [[ -n "$line" ]]; do
		if [[ $line != \#* ]] ; then
			if [[ $line = git* ]] ; then
				clone ${line}
			elif [[ $line = tar* ]] ; then
				download ${line}
			elif [[ $line = zip* ]] ; then
				download ${line}
			fi
		fi
	done < "${SCRIPT_PATH}/${SCRIPT_NAME}.lst"
fi

# ------------------------------------------------------------------------------
# DONE:
# ------------------------------------------------------------------------------

cd "$SCRIPT_PATH"
echo "> Done."