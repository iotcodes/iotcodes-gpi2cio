# /////////////////////////////////////////////////////////////////////////////
# IOTCODES.ORG
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being covered by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////

#!/bin/bash

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

CURRENT_DIR="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_DIR}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"

figlet -w 240 "${SCRIPT_NAME}..." 2> /dev/null
if [ $? -ne 0 ]; then
    banner "${SCRIPT_NAME}..." 2> /dev/null
    if [ $? -ne 0 ]; then
    	echo "> ${SCRIPT_NAME}:" | tr a-z A-Z 
	fi
fi

# ------------------------------------------------------------------------------
# FUNCTIONS:
# ------------------------------------------------------------------------------

function install {
	package=$1
	echo "> Installing package '${package}'..."
	result=$(apt-get -y install "${package}" 2>&1)
	if [ $? -ne 0 ]; then
		echo "${result}"
		echo "> Cannot install package '${package}' as of above error(s), aborting."
		exit 1
	fi
}

# ------------------------------------------------------------------------------

function probe {
	path=$(which $1 2> /dev/null )
	if [ $? -ne 0 ]; then
		echo "> Cannot invoke \"$1\", please install required tool \"$1\", aborting."
		exit 1 1
	fi
}

# ------------------------------------------------------------------------------

function ask {
	input=""
	while ([ "$input" !=  "n" ] && [ "$input" !=  "y" ] ); do
	  echo -n "> Continue? Enter [n] to quit, [y] to continue: "; read input;
	done
	if [ "$input" == "n" ] ; then
		echo "> Aborting due to user input."
		exit 1
	fi
}

# ------------------------------------------------------------------------------

function help {
	echo "Usage: $SCRIPT_NAME.sh [ -h ]"
	echo "   -h: Shows this help."
}

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------

args=$@
POSITIONAL=()
while [[ $# -gt 0 ]] ; do
	key="$1"
	case $key in
		-h|--help)
			help
			exit 0
		;;
		*)
			help
			echo "> Wrong (number of) arguments: \"$args\""
			exit 1
		;;
	esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

echo "> Installing required tools for debian based distros..."
ask

# ------------------------------------------------------------------------------
# INSTALL:
# ------------------------------------------------------------------------------

probe "apt"

while IFS='' read -r line || [[ -n "$line" ]]; do
	if [[ $line != \#* ]] ; then
		install ${line}
	fi
done < "${SCRIPT_PATH}/${SCRIPT_NAME}.lst"

# DONE:

cd "$SCRIPT_PATH"
echo "> Done."
