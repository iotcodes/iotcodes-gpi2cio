// /////////////////////////////////////////////////////////////////////////////
// IOTCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

#include <Arduino.h>
#include <GPI2CIOMaster.h>
#include "Master.h"

const unsigned long INTERVAL_IN_MILLIS = 2000;
unsigned long _previousTime = 0;

void setup() {
	Serial.begin(9600);
	while (!Serial) {
		delay(1);
	}
	Serial.println("> Master.setup() ...");
	GPI2CIOMaster::init();
}

void loop() {
	const unsigned long currentTime = millis();
	if (currentTime - _previousTime > INTERVAL_IN_MILLIS) {
		Serial.println("> Master.loop() ...");
		int result = GPI2CIOMaster::analogRead(8, 14);
		Serial.print("Master.loop(): result = ");
		Serial.println(result);
		result =  GPI2CIOMaster::analogRead(8, 15);
		Serial.print("Master.loop(): result = ");
		Serial.println(result);
		Serial.println();
		_previousTime = currentTime;
	}
}
