// /////////////////////////////////////////////////////////////////////////////
// IOTCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

#include "GPI2CIOSlave.h"

#include <Arduino.h>
#include "GPI2CIOMaster.h"

/**
 * To prevent platform specific issues such as little endian, big endian or
 * number of bytes per integer,	we serialize our values to char sequences
 * (string representation of the given value) when sending. Each platform
 * receiving the string representations (char sequence) of the given values
 * converts the char sequences back to a value as of the given platform's
 * constraints.
 */
namespace GPI2CIOSlave {

	int pin = -1;
	PinMode pinMode = PinMode::UNDEFINED;

// -------------------------------------------------------------------------

	void onRequest() {
#ifdef _DEBUG_
		Serial.println("GPI2CIOSlave.onRequest(): Starting ...");
#endif
		if (pin != -1 && pinMode != PinMode::UNDEFINED) {
			int value = -1;
			if (pinMode == PinMode::ANALOG) {
#ifdef _DEBUG_
				Serial.print("GPI2CIOSlave.onRequest(): pin (analog) = ");
				Serial.println(pin);
#endif
				value = analogRead(pin);
			} else if (pinMode == PinMode::DIGITAL) {
#ifdef _DEBUG_
				Serial.print("GPI2CIOSlave.onRequest(): pin (digital) = ");
				Serial.println(pin);
#endif
				value = digitalRead(pin);
			}
			char iBuffer[INT_BUFFER_LENGTH];
			dtostrf((float) value, INT_BUFFER_LENGTH, INT_MAX_DECIMAL_PLACES, iBuffer);
#ifdef _DEBUG_
			Serial.print("GPI2CIOSlave.onRequest(): value = ");
			Serial.print(value);
			Serial.print(" (\"");
			Serial.print(iBuffer);
			Serial.println("\")");
			Serial.print("GPI2CIOSlave: SINGLE_BUFFER_LENGTH = ");
			Serial.println(SINGLE_BUFFER_LENGTH);
#endif
			Wire.write(iBuffer, INT_BUFFER_LENGTH);
			Wire.flush();
			pin = -1;
			pinMode = PinMode::UNDEFINED;
		}
	}

// -------------------------------------------------------------------------

	void onReceive(int bytes) {
#ifdef _DEBUG_
		Serial.println("GPI2CIOSlave.onReceive(): Starting ...");
#endif
		char iBuffer[INT_BUFFER_LENGTH + 1];
		iBuffer[INT_BUFFER_LENGTH] = 0;
		// Read pin:
		if (Wire.available()) {
			Wire.readBytes(iBuffer, INT_BUFFER_LENGTH);
			pin = atoi(iBuffer);
#ifdef _DEBUG_
			Serial.print("GPI2CIOSlave.onReceive(): pin = ");
			Serial.println(pin);
#endif
		} else {
			pin = -1;
		}
		// Read pinMode:
		if (Wire.available()) {
			Wire.readBytes(iBuffer, INT_BUFFER_LENGTH);
			pinMode = atoi(iBuffer);
#ifdef _DEBUG_
			Serial.print("GPI2CIOSlave.onReceive(): pin mode = ");
			Serial.println(pinMode);
#endif
		} else {
			pinMode = PinMode::UNDEFINED;
		}
	}

	void init(int busAddr) {
#ifdef _DEBUG_
		Serial.print("GPI2CIOSlave.init(): Initializing ...");
#endif
		Wire.begin(busAddr);
		Wire.onRequest(GPI2CIOSlave::onRequest);
		Wire.onReceive(GPI2CIOSlave::onReceive);
#ifdef _DEBUG_
		Serial.println("GPI2CIOSlave.init(): busAddr = ");
		Serial.println(busAddr);
#endif
	}
}
