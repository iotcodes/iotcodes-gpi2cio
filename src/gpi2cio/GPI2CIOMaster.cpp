// /////////////////////////////////////////////////////////////////////////////
// IOTCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

#include "GPI2CIOMaster.h"

#include <Arduino.h>

/**
 * To prevent platform specific issues such as little endian, big endian or
 * number of bytes per integer,	we serialize our values to char sequences
 * (string representation of the given value) when sending. Each platform
 * receiving the string representations (char sequence) of the given values
 * converts the char sequences back to a value as of the given platform's
 * constraints.
 */
namespace GPI2CIOMaster {

	int analogRead(int busAddr, int pin) {
		return read(busAddr, pin, PinMode::ANALOG);
	}

// -------------------------------------------------------------------------

	int digitalRead(int busAddr, int pin) {
		return read(busAddr, pin, PinMode::DIGITAL);
	}

// -------------------------------------------------------------------------

	int read(int busAddr, int pin, PinMode pinMode) {
#ifdef _DEBUG_
		Serial.print("GPI2CIOMaster.read(): busAddr = ");
		Serial.println(busAddr);
		Serial.print("GPI2CIOMaster.read(): pin = ");
		Serial.println(pin);
		Serial.print("GPI2CIOMaster.read(): pinMode = ");
		switch (pinMode) {
		case PinMode::ANALOG:
			Serial.print("ANALOG");
			break;
		case PinMode::DIGITAL:
			Serial.print("DIGITAL");
			break;
		case PinMode::UNDEFINED:
			Serial.print("UNDEFINED");
			break;
		default:
			Serial.print("UNKNOWN");
		}
		Serial.print(" (");
		Serial.print(pinMode);
		Serial.println(")");
#endif
		char iBuffer[INT_BUFFER_LENGTH + 1];
		iBuffer[INT_BUFFER_LENGTH] = 0;
		// Send pin:
		Wire.beginTransmission(busAddr);
		dtostrf((float) pin, INT_BUFFER_LENGTH, INT_MAX_DECIMAL_PLACES, iBuffer);
		Wire.write(iBuffer, INT_BUFFER_LENGTH);
		// Send pin mode:
		dtostrf((float) pinMode, INT_BUFFER_LENGTH, INT_MAX_DECIMAL_PLACES, iBuffer);
		Wire.write(iBuffer, INT_BUFFER_LENGTH);
		Wire.endTransmission();
		// Send post process:
		Wire.flush();
		delay(500);
		// Receive result:
		Wire.requestFrom(busAddr, INT_BUFFER_LENGTH);
		int size = Wire.readBytes(iBuffer, INT_BUFFER_LENGTH);
		int value = atoi(iBuffer);
#ifdef _DEBUG_
		Serial.print("GPI2CIOMaster.digitalRead(): value = ");
		Serial.print(value);
		Serial.print(" (\"");
		Serial.print(iBuffer);
		Serial.println("\")");
		Serial.print("GPI2CIOMaster.digitalRead(): size = ");
		Serial.println(size);
		Serial.print("GPI2CIOMaster: INT_BUFFER_LENGTH = ");
		Serial.println(INT_BUFFER_LENGTH);
#endif
		return value;
	}

	void init() {
#ifdef _DEBUG_
		Serial.println("GPI2CIOMaster.init(): initializing ... ");
#endif
		Wire.begin();
	}
}
