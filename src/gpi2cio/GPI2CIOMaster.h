// /////////////////////////////////////////////////////////////////////////////
// IOTCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

#ifndef _I2C_MASTER_H_
#define _I2C_MASTER_H_

#define _DEBUG_
#include <Wire.h>

/*
 * To estimate the required buffer sizes for our case:
 *
 *                   | Single| Double | Extended | Quad  | Int | Long
 *                   |:-----:|:------:|:--------:|:-----:|:---:|:----:|
 * Number of digits: |    16 |     24 |       30 |    45 |  11 |   20 |
 *   Decimal places: |     9 |     17 |       21 |    36 |   0 |    0 |
 *
 * Conversion from a number to a string uses a fixed number of decimal places.
 * A number of a given type has a given maximum length in decimal digits and a
 * maximum number of decimal places. As conversion of a number without any
 * decimal places fills up the decimal places with zeros, the buffer must be the
 * maximum number of digits plus the maximum number of decimal places plus one
 * in case we have more than zero decimal places to expect.
 */

#define SINGLE_MAX_NUMBER_OF_DIGITS 16
#define SINGLE_MAX_DECIMAL_PLACES 9
#define SINGLE_BUFFER_LENGTH ( ( SINGLE_MAX_NUMBER_OF_DIGITS + SINGLE_MAX_DECIMAL_PLACES ) + ( SINGLE_MAX_DECIMAL_PLACES > 0 ? 1 : 0 ) )

// #define DOUBLE_MAX_NUMBER_OF_DIGITS 24
// #define DOUBLE_MAX_DECIMAL_PLACES 17
// #define DOUBLE_BUFFER_LENGTH ( ( DOUBLE_MAX_NUMBER_OF_DIGITS + DOUBLE_MAX_DECIMAL_PLACES ) + ( DOUBLE_MAX_DECIMAL_PLACES > 0 ? 1 : 0 ) )

// #define EXTENDED_MAX_NUMBER_OF_DIGITS 30
// #define EXTENDED_MAX_DECIMAL_PLACES 21
// #define EXTENDED_BUFFER_LENGTH ( ( EXTENDED_MAX_NUMBER_OF_DIGITS + EXTENDED_MAX_DECIMAL_PLACES ) + ( EXTENDED_MAX_DECIMAL_PLACES > 0 ? 1 : 0 ) )

// #define QUAD_MAX_NUMBER_OF_DIGITS 45
// #define QUAD_MAX_DECIMAL_PLACES 36
// #define QUAD_BUFFER_LENGTH ( ( QUAD_MAX_NUMBER_OF_DIGITS + QUAD_MAX_DECIMAL_PLACES ) + ( QUAD_MAX_DECIMAL_PLACES > 0 ? 1 : 0 ) )

#define INT_MAX_NUMBER_OF_DIGITS 11
#define INT_MAX_DECIMAL_PLACES 0
#define INT_BUFFER_LENGTH ( ( INT_MAX_NUMBER_OF_DIGITS + INT_MAX_DECIMAL_PLACES ) + ( INT_MAX_DECIMAL_PLACES > 0 ? 1 : 0 ) )

#define LONG_MAX_NUMBER_OF_DIGITS 20
#define LONG_MAX_DECIMAL_PLACES 0
#define LONG_BUFFER_LENGTH ( ( LONG_MAX_NUMBER_OF_DIGITS + LONG_MAX_DECIMAL_PLACES ) + ( LONG_MAX_DECIMAL_PLACES > 0 ? 1 : 0 ) )

enum PinMode {
	UNDEFINED,
	DIGITAL,
	ANALOG,
};

/*
 * IC2 bus-master implementation for issuing a request.
 */
namespace GPI2CIOMaster {

	/*
	 * Initializes the i2c-master.
	 */
	void init();

	/*
	 * Reads a remote pin's analog value from the I2C bus-address with the given pin.
	 */
	int analogRead(int busAddr, int pin);

	/*
	 * Reads a remote pin's binary value from the I2C bus-address with the given pin.
	 */
	int digitalRead(int busAddr, int pin);

	/*
	 * Reads a remote pin's value from the I2C bus-address with the given pin and pin mode.
	 */
	int read(int busAddr, int pin, PinMode pinMode);
}

#endif
