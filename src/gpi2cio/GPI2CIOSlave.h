// /////////////////////////////////////////////////////////////////////////////
// IOTCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

#ifndef _I2C_SLAVE_H_
#define _I2C_SLAVE_H_

#define _DEBUG_
#include <Wire.h>

/*
 * IC2 bus-slave implementation for processing a request.
 */
namespace GPI2CIOSlave {

	/*
	 * Initializes the i2c-slave for the given bus address.
	 */
	void init(int busAddr);
}

#endif
