# /////////////////////////////////////////////////////////////////////////////
# IOTCODES.ORG
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being covered by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////

#!/bin/bash

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

CURRENT_DIR="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_DIR}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"

figlet -w 240 "${SCRIPT_NAME}..." 2> /dev/null
if [ $? -ne 0 ]; then
    banner "${SCRIPT_NAME}..." 2> /dev/null
    if [ $? -ne 0 ]; then
    	echo "> ${SCRIPT_NAME}:" | tr a-z A-Z 
	fi
fi

# ------------------------------------------------------------------------------
# FUNCTIONS:
# ------------------------------------------------------------------------------

function probe {
	path=$(which $1 2> /dev/null )
	if [ $? -eq 0 ]; then
	    printf "[\033[1;32mOK\033[0m] \033[1m%-8s\033[0m - Location is \"${path}\"\n" $1
		if [ ! -z "$2" ] ; then
		    version $1 $2
        fi
		return
	else
		printf "[\033[1;31mNO\033[0m] \033[1m%-8s\033[0m - Please install \"$1\"\n" $1
		return 1
	fi
}

# ------------------------------------------------------------------------------

function version {
    tmp=$($1 -version 2>&1>/dev/null)
    if [ $? -eq 0 ]; then
    	version=$("$1" -version 2>&1 | sed 's/[^0-9.]*\([0-9.]*\).*/\1/' | grep '[0-9]' | head -n 1 )
    	# version=$("$1" -version 2>&1 | awk -F '"' '/version/ {print $2}')
        # version=$($1 -version 2>&1 | sed 's/$1 version "\(.*\)\.\(.*\)\..*"/\1\2/; 1q' | cut -f1 -d"(" | cut -f1 -d"_" | cut -f2 -d"]" | tr -d [:alpha:] )
        # version=${version//\"}
        # version=${version// }
        if [[ "${version}" < "$2" ]] ; then
            printf "[\033[1;31mNO\033[0m] \033[1m%-8s\033[0m - Found version \"\033[1m${version}\033[0m\", required version is \"\033[1m$2\033[0m\"\n" $1
			return 1
        fi
        printf "[\033[1;32mOK     \033[0m]   \033[1m%-8s\033[0m - Version is \"\033[1m${version}\033[0m\"\n" $1
		return
    else
        printf "[\033[1;33mWARN   \033[0m]   \033[1m%-8s\033[0m - No version information found, required version is \"\033[1m$2\033[0m\"\n" $1
		return
    fi
    return
}

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------

echo ""
while IFS='' read -r line || [[ -n "$line" ]]; do
    cmd=$( echo $line | cut -d";" -f1 )
    ver=$( echo $line | cut -d";" -f2 )
    if [[ "$cmd" == "$ver" ]] ; then
		if ! probe $cmd ; then 
			failed="true"
		fi;
    else
		if ! probe $cmd $ver ; then
			failed="true"
		fi;
    fi
done < "${SCRIPT_PATH}/${SCRIPT_NAME}.txt"

echo ""
if [ "$failed" == "true" ]; then
	echo "> Please install the above missing commands or required versions."
else
	echo "> Everything is OK."
	exit
fi
exit $exitCode
