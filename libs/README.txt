--------------------------------------------------------------------------------
This folder for external libraries is not part of the actual project!
--------------------------------------------------------------------------------
Any files found here are used to patch, pre- ore post-process the according 
external libraries. 
-------------------------------------------------------------------------------
DO NOT RENAME THE FILES FOUND IN THIS FOLDER! 
-------------------------------------------------------------------------------
The filenames corresponds to the folders to be patched, pre- or post-processed!
Install the external libraries by invoking the script 'install-libs.sh' found in
the root folder of this project.
--------------------------------------------------------------------------------
MAINTAINER: To create a patch file, go as follows:
--------------------------------------------------------------------------------
$ cd ./libs/<library>
$ diff -c src/path/file.old src/path/file.txt > ../<library>.patch
# You end up with a file ./libs/<library>.patch
--------------------------------------------------------------------------------