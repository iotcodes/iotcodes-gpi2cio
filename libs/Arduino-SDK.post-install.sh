# ------------------------------------------------------------------------------
# DO NOT RENAME ME! The filename corresponds to the folder to be post-processed!
# ------------------------------------------------------------------------------

#!/bin/bash

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

CURRENT_DIR="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_DIR}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"

figlet -w 240 "${SCRIPT_NAME}..." 2> /dev/null
if [ $? -ne 0 ]; then
    banner "${SCRIPT_NAME}..." 2> /dev/null
    if [ $? -ne 0 ]; then
    	echo "> ${SCRIPT_NAME}:" | tr a-z A-Z 
	fi
fi

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------

LIB_NAME="${SCRIPT_NAME%.post-install}"
LIB_PATH="${SCRIPT_PATH}/${LIB_NAME}"
if [ -f "${LIB_PATH}/install.sh" ] ; then
	cd "${LIB_PATH}"
	./install.sh
	cd "${CURRENT_DIR}"
fi