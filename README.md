# README #

> "*The [`IOTCODES.ORG`](http://www.iotcodes.org) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.*"

### What is this repository for? ###

The [`iotcodes-gpi2cio`](https://bitbucket.org/iotcodes/iotcodes-gpi2cio) sources provide means to use an [Arduino](https://en.wikipedia.org/wiki/Arduino)'s [`GPIO`](https://en.wikipedia.org/wiki/General-purpose_input/output) pins remotely by another [Arduino](https://en.wikipedia.org/wiki/Arduino) using the [`I2C`](https://en.wikipedia.org/wiki/I%C2%B2C) bus.

### How do I get set up? ###

This is a [CMake](https://en.wikipedia.org/wiki/CMake) based project. Using the [Arduino IDE](https://en.wikipedia.org/wiki/Arduino_IDE) you might go for the folder [`./src/gpi2cio`](https://bitbucket.org/iotcodes/iotcodes-gpi2cio/src/master/src/gpi2cio/) and proceed as described in [Arduino Libraries}(https://www.arduino.cc/en/hacking/libraries). A small example on the usage is found in the files [`Master.cpp`](https://bitbucket.org/iotcodes/iotcodes-gpi2cio/src/master/src/Master.cpp) and [`Slave.cpp`](https://bitbucket.org/iotcodes/iotcodes-gpi2cio/src/master/src/Slave.cpp).

### Contribution guidelines ###

* Package an Arduino IDE library
* Code review
* Other guidelines

### Who do I talk to? ###

* Siegfried Steiner (steiner@iotcodes.org)

### Terms and conditions ###

The [`IOTCODES.ORG`](http://www.iotcodes.org) group of artifacts is published under some open source licenses; covered by the  [`iotcodes-licensing`](https://bitbucket.org/iotcodes/iotcodes-licensing) ([`org.iotcodes`](https://bitbucket.org/iotcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.